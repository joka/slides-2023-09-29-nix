{ fetchFromGitHub, mkDerivation }:

mkDerivation rec {
  pname = "revealjs";
  version = "4.5.0";

  src = fetchFromGitHub {
    owner = "hakimel";
    repo = "reveal.js";
    rev = "refs/tags/${version}";
    hash = "sha256-9Q7aWgjAV37iJp6oYDz45e8J+RKwKY1Uvgg/BXwf5nQ=";
  };

  dontBuild = true;
  dontPatch = true;
  dontConfigure = true;
  dontFixup = true;

  installPhase = ''
    mkdir -vp $out
    cp -r * $out
  '';
}
