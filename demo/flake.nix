{
  description = "Demo for presentation";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";

  outputs = { self, nixpkgs, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      my-python = pkgs.python310.withPackages (py-pkgs: with py-pkgs; [
        numpy
        pandas
        jupyter
      ]);
    in
    {
      packages.${system} = {
        my-python = my-python;
        default = pkgs.hello;
      };
      devShells.${system}.default = pkgs.mkShell {
        buildInputs = [
          my-python
        ];
      };
    };
}
